# -*- coding: utf-8 -*-

from sqlalchemy import Column
from sqlalchemy import String, Integer, LargeBinary
from sqlalchemy.dialects.postgresql import JSONB

from app.models import Base
from app.config import UUID_LEN
from app.utils import alchemy


class User(Base):
    user_id = Column(Integer, primary_key=True)
    username = Column(String(20), nullable=False)
    firstname = Column(String(100), nullable=True, default="Antony")
    lastname = Column(String(100), nullable=True, default="Mwathi")
    email = Column(String(320), unique=True, nullable=False)
    password = Column(String(80), nullable=False)
    info = Column(JSONB, nullable=True)
    token = Column(String(255), nullable=False)

    # intentionally assigned for user related service such as resetting password: kind of internal user secret key
    sid = Column(String(UUID_LEN), nullable=False)

    def __repr__(self):
        return "<User(username='%s', email='%s', token='%s', info='%s', firstname='%s', lastname='%s')>" % (
            self.username,
            self.email,
            self.token,
            self.info,
            self.firstname,
            self.lastname
        )

    @classmethod
    def get_id(cls):
        return User.user_id

    @classmethod
    def find_by_email(cls, session, email):
        return session.query(User).filter(User.email == email).one()

    FIELDS = {"username": str, "email": str, "info": alchemy.passby, "token": str}

    FIELDS.update(Base.FIELDS)
